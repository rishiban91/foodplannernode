"use strict";
module.exports = () => {
  return {
    port: 8082,
    logCacheMisses: true,
    logLevel: "info"
  };
};

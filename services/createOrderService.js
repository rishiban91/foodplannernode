"use strict";
const db = require("mssql");

module.exports = (log, config) => {
  return {
    insertOrder: (req, res) => {
      const request = new db.Request();
      const insertQuery =
        "INSERT INTO FOOD_ORDER ( STATUS, TOTAL, NOTES, NICK_NAME, PHONE, STORE_CODE, ACTIVE)" +
        " VALUES(@status, @total, @notes, @nickName, @phone, @storecode, 'Y'); SELECT @@IDENTITY AS ID";
      request
        .input("status", req.body.status)
        .input("total", req.body.total)
        .input("notes", req.body.notes)
        .input("nickName", req.body.nickName)
        .input("phone", req.body.phone)
        .input("storecode", req.body.storeCode)
        .query(insertQuery, function(err, result) {
          if (err) {
            console.log(err);
          }

          let orderRes = result.recordset[0];
          let orderId = orderRes.ID;

          let items = req.body.items;

          const itemInsertQuery =
            "INSERT INTO ORDER_ITEMS ( ORDER_ID, PRODUCT_ID, QUANTITY, ACTIVE )" +
            "VALUES ( @orderId, @productId, @quantity, 'Y' )";

          req.body.items.map(item => {
            const request = new db.Request();
            request
              .input("orderId", orderId)
              .input("productId", item.productId)
              .input("quantity", item.quantity)
              .query(itemInsertQuery, function(err, result) {
                if (err) {
                  console.log(err);
                }
              });
          });
          res.send(JSON.stringify(result.recordset[0]));
        });
    }
  };
};

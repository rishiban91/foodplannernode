"use strict";
const db = require("mssql");

module.exports = (log, config) => {
  return {
    deleteProd: (req, res) => {
      const request = new db.Request();
      /* log.debug("calling getProducts with key " + req.body);
      if (req.body === "illegal key") {
        log.error("illegal key");
        return null;
      } */
      const deleteProdQuery = "DELETE FROM PRODUCT WHERE ID =@id";
      return request
        .input("id", req.body.id)
        .query(deleteProdQuery, function(err, resultSet) {
          if (err) {
            console.log(err);
          }
          console.log(resultSet);
          /* res.send(JSON.stringify()); */
          res.send(resultSet);
        });
    }
  };
};

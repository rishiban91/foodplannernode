"use strict";
const db = require("mssql");

module.exports = (log, config) => {
  return {
    updateOrder: (req, res) => {
      const request = new db.Request();
      const query = "UPDATE FOOD_ORDER SET STATUS =@status WHERE ID=@id";
      request
        .input("status", req.body.status)
        .input("id", req.body.id)
        .query(query, function(err, resultSet) {
          if (err) {
            console.log(err);
          }
          res.send(resultSet);
        });
    }
  };
};

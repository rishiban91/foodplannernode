"use strict";
const db = require("mssql");

module.exports = (log, config) => {
  return {
    getProducts: (categoryCode, res) => {
      const request = new db.Request();
      log.debug("calling getProducts with key " + categoryCode);
      if (categoryCode === "illegal key") {
        log.error("illegal key");
        return null;
      }

      let productListQuery;

      if (categoryCode) {
        /* productListQuery = "SELECT * FROM DBO.PRODUCT WHERE CATEGORY_CODE = @catCode AND ACTIVE = 'Y'"; */
        productListQuery =
          "SELECT P.ID, P.CATEGORY_CODE, P.NAME, P.DESCRIPTION, P.IMAGE_URL, P.PRICE, P.QUANTITY, N.NAME, N.CALORIES" +
          "FROM PRODUCT AS P LEFT JOIN NUTRITION AS N ON P.ID=N.PRODUCT_ID WHERE P.CATEGORY_CODE =@catCode AND P.ACTIVE='Y'";
        request.input("catCode", categoryCode);
      } else {
        productListQuery = "SELECT * FROM DBO.PRODUCT WHERE ACTIVE = 'Y'";
      }

      request.query(productListQuery, function(err, resultSet) {
        if (err) {
          console.log(err);
        }

        let rs = resultSet.recordset;
        console.log(resultSet);
        let productList = [];
        let prevId = 0;
        let product = undefined;

        for (let i = 0; i < rs.length; i++) {
          let pr = rs[i];
          let currId = pr.ID;
          if (prevId != currId) {
            if (product) {
              productList.push(product);
            }
            let product = {};
            product.id = pr.ID;
            product.categoryCode = pr.CATEGORY_CODE;
            product.name = pr.NAME;
            product.description = pr.DESCRIPTION;
            product.imageUrl = pr.IMAGE_URL;
            product.price = pr.PRICE;
            product.created = pr.CREATED;
            product.quantity = pr.QUANTITY;
            product.nutrients = [];
            prevId = currId;
          }
          let nutrient = {};
          nutrient.name = pr.NAME;
          nutrient.calories = pr.CALORIES;
          if (product.nutrients) {
            product.nutrients.push(nutrient);
          }
          productList.push(product);
          res.send(JSON.stringify(productList));
        }
      });
    }
  };
};

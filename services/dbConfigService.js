"use strict";

module.exports = db => {
  const config = {
    user: "FOOD_PLANNER_APP",
    password: "f00dp1ann3rapp",
    server: "PTPSEELM-NT2008.ikeadt.com",
    database: "FOOD_PLANNER_APP",
    options: {
      encrypt: true
    }
  };
  const connection = db.connect(
    config,
    function(err) {
      if (err) console.log(err);
    }
  );

  return {
    config,
    connection
  };
};

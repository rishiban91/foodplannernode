"use strict";
const db = require("mssql");

module.exports = (log, config) => {
  return {
    deleteOrderById: (id, res) => {
      const request = new db.Request();
      log.debug("calling getProducts with key " + id);
      if (id === "illegal key") {
        log.error("illegal key");
        return null;
      }
      const deleteOrderQuery = "DELETE * FROM DBO.FOOD_ORDER WHERE ID =@id ";
      request.input("id", id).query(deleteOrderQuery, function(err, resultSet) {
        if (err) {
          console.log(err);
        }
        const deleteItemQyery = "DELETE *FROM DBO.ORDER_ITEMS WHERE ID =@id";
        request
          .input("id", id)
          .query(deleteItemQyery, function(err, resultSet) {
            if (err) console.log(err);
            //console.log(resultSet);
          });

        res.send(JSON.stringify(resultSet));
      });
    }
  };
};

"use strict";
module.exports = (log, dbConfig) => {
  const prodSvc = require("./productService")(log, dbConfig);
  const catSvc = require("./categoryService")(log, dbConfig);
  const orderSvc = require("./orderService")(log, dbConfig);
  const createOrderSvc = require("./createOrderService")(log, dbConfig);
  const deleteOrderSvc = require("./deleteOrderServiceById")(log, dbConfig);
  /* const addCustomerSvc = require("./addCutomerService")(log, dbConfig); */
  const kitchenDashboardSvc = require("./getOrders")(log, dbConfig);
  const addProdSvc = require("./addProductService")(log, dbConfig);
  const deleteProdSvc = require("./deleteProductService")(log, dbConfig);
  /*  const deliveryDashboardSvc = require("./deliveryDashboardService")(
    log,
    dbConfig
  ); */
  const updateFoodOrderSvc = require("./updateFoodOrderService")(log, dbConfig);

  return {
    prodSvc,
    catSvc,
    orderSvc,
    createOrderSvc,
    deleteOrderSvc,
    kitchenDashboardSvc,
    updateFoodOrderSvc,
    addProdSvc,
    deleteProdSvc
    /* deliveryDashboardSvc */
  };
};

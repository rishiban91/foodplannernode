"use strict";
const db = require("mssql");

module.exports = (log, config) => {
  return {
    getCategories: (req, res) => {
      const request = new db.Request();
      const query = "SELECT * FROM DBO.CATEGORY WHERE ACTIVE = 'Y'";
      request.query(query, function(err, resultSet) {
        if (err) {
          console.log(err);
        }
        let catList = [];
        for (let i = 0; i < resultSet.recordset.length; i++) {
          let ct = resultSet.recordset[i];
          let cat = {};
          cat.id = ct.ID;
          cat.name = ct.NAME;
          cat.description = ct.DESCRIPTION;
          catList.push(cat);
        }
        console.log(catList);
        res.send(JSON.stringify(catList));
      });
    }
  };
};

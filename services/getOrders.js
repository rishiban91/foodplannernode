"use strict";
const db = require("mssql");

module.exports = (log, config) => {
  return {
    getKitchenOrder: (req, res) => {
      const request = new db.Request();

      const kitchenOrderQuery =
        "SELECT FO.ID, FO.STATUS, FO.NOTES, FO.NICK_NAME,FO.CREATED,   P.NAME , P.PRICE, OI.QUANTITY FROM ORDER_ITEMS AS OI " +
        "INNER JOIN PRODUCT AS P ON OI.PRODUCT_ID=P.ID " +
        "INNER JOIN FOOD_ORDER AS FO ON FO.ID=OI.ORDER_ID " +
        "WHERE FO.STATUS IN (@status) ORDER BY ORDER_ID";

      /*  const statusArray = req.body;

      let statusStr = "'";

      for (let i = 0; i < statusArray.length; i++) {
        statusStr = statusStr + statusArray[i] + "', ";
      }

      console.log(statusStr, "status string"); */

      return request
        .input("status", "Placed")
        .query(kitchenOrderQuery, function(err, resultSet) {
          if (err) {
            console.log(err);
          }

          let rs = resultSet.recordset;

          let orderList = [];
          let prevId = 0;
          let order = undefined;

          for (let i = 0; i < rs.length; i++) {
            let data = rs[i];
            let currId = data.ID;
            if (prevId != currId) {
              if (order) {
                orderList.push(order);
              }
              order = {};
              order.id = data.ID;
              order.status = data.STATUS;
              order.notes = data.NOTES;
              order.nickName = data.NICK_NAME;
              order.items = [];
              order.create = data.CREATED;
              prevId = currId;
            }
            let item = {};
            item.name = data.NAME;
            item.price = data.PRICE;
            item.quantity = data.QUANTITY;
            if (order.items) {
              order.items.push(item);
            }
          }
          orderList.push(order);
          res.send(JSON.stringify(orderList));
        });
    }
  };
};

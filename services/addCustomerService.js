"use strict";
const db = require("mssql");

module.exports = (log, config) => {
  return {
    insertIntoContact: (req, res) => {
      const request = new db.Request();
      //const createOrder ="INSERT INTO FOOD_ORDER SET ?";

      const insertContactQuery =
        "INSERT INTO FOOD_ORDER ( ORDER_ID, PHONE, , ACTIVE)" +
        " VALUES(@status, @total, @notes, @nickName, @phone, @storecode, 'Y'); SELECT @@IDENTITY AS ID";
      request
        .input("status", req.body.status)
        .input("total", req.body.total)
        .input("notes", req.body.notes)
        .input("nickName", req.body.nickName)
        .input("phone", req.body.phone)
        .input("storecode", req.body.storeCode)
        .query(insertContactQuery, function(err, result) {
          if (err) {
            console.log(err);
          }

          let orderRes = result.recordset[0];
          let orderId = orderRes.ID;

          let items = req.body.items;

          const itemInsertQuery =
            "INSERT INTO ORDER_ITEMS ( ORDER_ID, PRODUCT_ID, QUANTITY, ACTIVE )" +
            "VALUES ( @orderId, @productId, @quantity, 'Y' );";

          req.body.items.map(item => {
            return request
              .input("orderId", orderId)
              .input("productId", item.productId)
              .input("quantity", item.quantity)
              .query(itemInsertQuery, function(err, result) {
                if (err) {
                  console.log(err);
                }
              });
          });
          /* for (let i = 0; i < items.length; i++) {
            let item = items[i];
            request
              .input("orderId", orderId)
              .input("productId", items[i].productId)
              .input("quantity", items[i].quantity)
              .query(itemInsertQuery, function(err, result) {
                if (err) {
                  console.log(err);
                }
              });
          } */
          res.send(JSON.stringify(result.recordset[0]));
        });
    }
  };
};

"use strict";
const db = require("mssql");

module.exports = (log, config) => {
  return {
    getOrders: (id, res) => {
      const request = new db.Request();
      log.debug("calling getProducts with key " + id);
      if (id === "illegal key") {
        log.error("illegal key");
        return null;
      }
      const ordersListQuery =
        "SELECT FO.STATUS, FO.SUBTOTAL, FO.TAX, FO.TOTAL, FO.NOTES, FO.NICK_NAME," +
        " FO.PHONE, P.NAME, P.DESCRIPTION, OI.QUANTITY, P.PRICE " +
        "FROM FOOD_ORDER AS FO INNER JOIN  ORDER_ITEMS AS OI ON FO.ID=ORDER_ID" +
        " INNER JOIN PRODUCT AS P ON OI.PRODUCT_ID = P.ID WHERE FO.ID=@id AND FO.ACTIVE='Y'";
      request.input("id", id).query(ordersListQuery, function(err, resultSet) {
        if (err) {
          console.log(err);
        }

        let rs = resultSet.recordset;

        let order = {};

        for (let i = 0; i < rs.length; i++) {
          let ordersData = rs[i];
          if (i === 0) {
            order.status = ordersData.STATUS;
            order.subTotal = ordersData.SUBTOTAL;
            order.notes = ordersData.NOTES;
            order.tax = ordersData.TAX;
            order.total = ordersData.TOTAL;
            order.nickName = ordersData.NICK_NAME;
            order.phone = ordersData.PHONE;
            order.storeCode = ordersData.STORE_CODE;
            order.quantity = ordersData.QUANTITY;
            order.items = [];
          }
          let item = {};
          item.name = ordersData.NAME;
          item.quantity = ordersData.QUANTITY;
          item.price = ordersData.PRICE;
          order.items.push(item);
        }

        res.send(JSON.stringify(order));
      });
    }
  };
};

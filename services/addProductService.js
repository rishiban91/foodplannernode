"use strict";
const db = require("mssql");

module.exports = (log, config) => {
  return {
    addProducts: (req, res) => {
      const request = new db.Request();
      log.debug("calling getProducts with key " + req.body);
      if (req.body === "illegal key") {
        log.error("illegal key");
        return null;
      }
      const addProdQuery =
        "INSERT INTO PRODUCT (CATEGORY_CODE, NAME, DESCRIPTION, IMAGE_URL, PRICE, QUANTITY, ACTIVE)" +
        "VALUES (@catCode, @name, @description, @imgUrl,@price, @quantity, 'Y')";

      request
        .input("catCode", req.body.catCode)
        .input("name", req.body.name)
        .input("description", req.body.description)
        .input("imgUrl", req.body.imgurl)
        .input("price", req.body.price)
        .input("quantity", req.body.quantity)
        .query(addProdQuery, function(err, resultSet) {
          if (err) {
            console.log(err);
          }
          /* req.body.products.map(product =>{
            const request = new db.Request();
            request
            .input("catCode", product.catCode)
            .input("name", product.name)
            .input("description", product.description)
            .input("imgUrl", product.imgUrl)
            .input("price", product.price)
            .input("quantity", product.quantity)
            .query(addProdQuery, function(err, resultSet) {
              if (err) {
                console.log(err);
              }
          }) */

          console.log(resultSet);
          /* res.send(JSON.stringify()); */
        });
    }
  };
};

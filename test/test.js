const request = require("supertest");
const server = require("../server");
const product = require("../router");

describe("DefaultRoute", function() {
  it("It should return some default message", done => {
    request(product)
      .get("/")
      .expect(200)
      .expect(
        /This is default route. Use find:key to find data, or clearcache to clear cache./,
        done
      );
  });
});

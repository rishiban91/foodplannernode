"use strict";
module.exports = deleteOrderSvc => {
  return (req, res) => {
    const id = req.params.id;
    console.log(id);
    deleteOrderSvc.deleteOrderById(id, res);
  };
};

'use strict';
module.exports = (prodSvc) => {
    return (req,res) => {
        const categoryCode = req.params.categoryCode;
        console.log(categoryCode)
        let productList = [];
        prodSvc.getProducts(categoryCode, res);
    };
};
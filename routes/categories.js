"use strict";
module.exports = dataService => {
  return (req, res) => {
    dataService.getCategories(req, res);
  };
};

"use strict";
module.exports = kitchenDashboardSvc => {
  return (req, res) => {
    kitchenDashboardSvc.getKitchenOrder(req, res);
  };
};

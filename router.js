"use strict";
module.exports = (dir, services) => {
  const router = require("express").Router();
  router.get("/", require(dir + "defaultRoute.js")());
  router.get(
    "/products/:categoryCode",
    require(dir + "products.js")(services.data.prodSvc)
  );
  router.post(
    "/addProducts",
    require(dir + "addProducts.js")(services.data.addProdSvc)
  );
  router.put(
    "/updateProduct",
    require(dir + "updateProduct.js")(services.data.updateProdSvc)
  );
  router.delete(
    "/deleteProduct",
    require(dir + "deleteProduct.js")(services.data.deleteProdSvc)
  );
  router.get(
    "/categories",
    require(dir + "categories.js")(services.data.catSvc)
  );
  router.get("/orders/:id", require(dir + "orders.js")(services.data.orderSvc));
  router.post(
    "/createOrder",
    require(dir + "createOrder.js")(services.data.createOrderSvc)
  );
  router.delete(
    "/deleteOrder",
    require(dir + "deleteOrder.js")(services.data.deleteOrderSvc)
  );
  /*  router.post("/addCustomer",require(dir+"addCustomer.js")(services.data.addCustomerSvc)) */
  router.post(
    "/getKitchenDashboard",
    require(dir + "kitchenDashboard.js")(services.data.kitchenDashboardSvc)
  );
  router.put(
    "/updateFoodOrderStatus",
    require(dir + "updateFoodOrder.js")(services.data.updateFoodOrderSvc)
  );

  return router;
};

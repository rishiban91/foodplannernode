"use strict";
module.exports = dir => {
  const db = require("mssql");
  const config = require(dir + "/serverConfigService")();
  const dbConfig = require(dir + "/dbConfigService")(db);
  const log = require(dir + "/logService")(config);
  const data = require(dir + "/dataService")(log, dbConfig);
  return {
    config,
    log,    
    data,
    dbConfig
  };
};
